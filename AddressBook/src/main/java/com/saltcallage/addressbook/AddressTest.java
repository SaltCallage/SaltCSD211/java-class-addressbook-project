/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saltcallage.addressbook;

import com.saltcallage.contactclasses.ContactBook;
import com.saltcallage.contactclasses.Contact;
import java.util.List;
/**
 *
 * @author mike
 */
public class AddressTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ContactBook testBook = new ContactBook();
        
        Contact[] sampleData = new Contact[10];
        
        sampleData[0] = new Contact("Ray", "Jones", "555-251-2851", "ray@jones.com");
        sampleData[1] = new Contact("Alex", "Jones", "555-661-2271", "AlexJones@jones.com");
        sampleData[2] = new Contact("Bill", "Ford", "555-221-2251", "BillyBoy@StevesComps.com");
        sampleData[3] = new Contact("Ray", "Jones", "555-221-2612", "raymon@DoubleJones.com");
        sampleData[4] = new Contact("Linden", "Stevens", "555-221-6271", "L.S@Stevens.edu");
        sampleData[5] = new Contact("Mike", "Michaels", "555-221-2211", "MM@MM.MM");
        sampleData[6] = new Contact("Steve", "Stone", "555-221-2331", "Stevey@StonesSomething.com");
        sampleData[7] = new Contact("Ray", "Ford", "555-263-2671", "OhDear@FordFord.com");
        sampleData[8] = new Contact("Pedo", "Bear", "555-663-6611", "Youknow@Theway.com");
        sampleData[9] = new Contact("Goku", "Ginka", "555-226-2267", "Goku@kamehameha.com");
       
        for(Contact element : sampleData)
        {
            testBook.RegisterContact(element);
        }
        
  	System.out.println("\n---Initiating sort by last name and print test---");      
        testBook.PrintAllContactsByLastName();
        System.out.println("--- END of sort by last name test ---");
        
        System.out.println("\n---Initiating repair the stone phone test---");
        Contact bear = testBook.GetContact(6);
        System.out.println("Original Entry:");
        bear.PrintReport();
        bear.UpdateContact(new Contact.Phone("555-999-9999"));
        System.out.println("New Entry:");
        bear.PrintReport();
        System.out.println("---End of repair stone phone test---");
        
        System.out.println("\n--- Initializing FIND RAY test ---");
        List<Contact> Rays = testBook.GetFirstName("Ray");
        
        for(Contact element : Rays)
        {
            element.PrintReport();
        }
        System.out.println("--- END of FIND RAY test ---");
        
                
    }
    
}
