/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saltcallage.contactclasses;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author mike
 */
public class ContactBook {
    private List<Contact> AllContacts = new ArrayList<Contact>();

    public void RegisterContact(Contact newContact)
    {
        AllContacts.add(newContact);
    }
    
    public void RemoveContact(Contact toRemove)
    {
        AllContacts.remove(toRemove);
    }
    
    public List<Contact> GetAllContacts()
    {
        return AllContacts;
    }
    
    public Contact GetContact(int cid)
    {
        if(cid < AllContacts.size())
        {
            return AllContacts.get(cid);
        }
        else
        {
            return null;
        }
    }
    
    //We're returning a list of Contacts with this function
    public List<Contact> GetFirstName(String firstName)
    {
        //We have to set up our new list
        List <Contact> foundContacts = new ArrayList<Contact>();
        //This is an enhanced for loop, looking through all contacts
        for(Contact contact : AllContacts)
        {
            //Remember what contact.GetReport does, what is element 0?
            //What is equalsIgnoreCase? a method call as a part of String.
            if(contact.GetReport()[0].equalsIgnoreCase(firstName))
                foundContacts.add(contact);
        }
        return foundContacts;
    }
    
    public void PrintAllContactsByLastName()
    {
        ArrayList<String> lastNames = new ArrayList<String>();
        for(Contact element : AllContacts)
        {
            String[] cd = element.GetReport();
            lastNames.add(cd[1] + ", " + cd[0] + ":" + cd[2] + "(" + AllContacts.indexOf(element) + ")");
        }
        Collections.sort(lastNames);
        
        for(String element : lastNames)
            System.out.println(element);
    }
}
