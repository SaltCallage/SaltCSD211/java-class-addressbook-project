/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saltcallage.contactclasses;

/**
 *
 * @author mike
 */
public class Contact {

    public Contact(String firstName, String lastName,String cPhone, String cEmail)
    {
        contactName = new Name(firstName, lastName);
        contactPhone = new Phone(cPhone);
        contactEmail = new Email(cEmail);
    }
    

    
    static public class Name{
        private String[] value = new String[2];
        
        public Name(String first, String last)
        {
            value[0] = first;
            value[1] = last;
        }
        
        public String GetFullName()
        {
            return value[0] + " " + value[1];
        }
        public String GetFirstName()
        {
            return value[0];
        }
        public String GetLastName()
        {
            return value[1];
        }
        
        public void SetName(String first, String last)
        {
            if(first != null)
                value[0] = first;
            if(last != null)
                value[1] = last;
        }
    }
    
    static public class Phone{
        private String value;
        
        public Phone(String number)
        {
            value = number;
        }
        
        public String GetNumber()
        {
            return value;
        }
        
        public void SetNumber(String newNumber)
        {
            value = newNumber;
        }
    }
    
    
    static public class Email{
        private String value;
        
        public Email(String email)
        {
            value = email;
        }
        public String GetAddress()
        {
            return value;
        }
        
        public void SetAddress(String newAddress)
        {
            value = newAddress;
        }
    }

    private Name contactName;
    private Phone contactPhone;
    private Email contactEmail;
    
    public void UpdateContact(Name newName)
    {
        contactName = newName;
    }
    public void UpdateContact(Email newEmail)
    {
        contactEmail = newEmail;
    }
    public void UpdateContact(Phone newPhone)
    {
        contactPhone = newPhone;
    }
    
    public String[] GetReport()
    {
        String[] ContactData = new String[4];
        ContactData[0] = contactName.GetFirstName();
        ContactData[1] = contactName.GetLastName();
        ContactData[2] = contactPhone.GetNumber();
        ContactData[3] = contactEmail.GetAddress();
        return ContactData;
    }
    
    public void PrintReport()
    {
        String[] contact = GetReport();
        System.out.println("Contact Name: \t" + contact[0] + contact[1]);
        System.out.println("Contact Phone: \t" + contact[2]);
        System.out.println("Contact Email: \t" + contact[3]);
    }
}
