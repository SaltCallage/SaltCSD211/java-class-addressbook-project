
# Using Classes to Make an Address Book in Java

**Before you begin you should know:**
- How to make a project in the Jave IDE of your choice.
- How to create packages and add java classes to them

**Step One: Create a new Project**
I leave this in your hands, name it as you will, get yourself started.
```
In my case, I created a new project called com.saltcallage.addressbook
```

**Step Two: Add a new package to hold the addressbook classes**
There are going to be two java classes that go into here, one for the book and one for the contacts that will fill it. Name it something like your.namespace.contactclasses
```
In my case I created a new package called com.saltcallage.contactclasses
```

**Step Three: Create a class for contacts**
Inside of the package you've just created add a new class called Contact. If you are using a fancy IDE it should put in some boilerplate code for you.
```java
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saltcallage.contactclasses;

/**
 *
 * @author mike
 */
public class Contact {
    //*** INSERTION POINT FOR NEW CODE *** 
    //Our class code is going in here.
}
```

## So what is a contact?

**For our purposes we will define a contact as the following:**
- A first name
- A last name
- A phone number
- an email address 

This is the information about a contact that gets stored in a variable, these are the properties of the object.

In the case of the contact, there are also operations that we will want to perform on specific contacts.
- Changing the names of contacts
- Changing the phone number
- Changing the email address

We are going to define classes for each of these pieces of information and create an `UpdateContact()` function that can take different types of data and update it without having to specify something like `UpdateContact(firstname, lastname, phonenumber, email);` or making a differently named function for each possibility.

Keep in mind that this may not be the best way to solve this problem, but we're going to use this to show the power of classes.
**Add Three new classes inside of the Contact class**
A class for each of our new types of information about a contact. We're making these classes a member of of the Contact class and so if we want to get at these classes, we will have to get at them through a contact object.
```java
public class Contact {
    static public class Name{
        
    }
    
    static public class Phone{
        
    }
    
    
    static public class Email{
        
    }
}
```
**See how we've made new types**
These are NOT the properties that we wanted to add to our contact, instead these new classes Name, Phone, Email will be templates for the properties that our contacts have. Let's create the properties now to demonstrate how this works. Take a look at the code I've added below my empty class declarations in the example below:
```java
public class Contact {
    static public class Name{
        
    }
    
    static public class Phone{
        
    }
    
    
    static public class Email{
    
    }

    private Name contactName;
    private Phone contactPhone;
    private Email contactEmail;
}
```
Your IDE should recognize Name, Phone and Email as actual types and the variables should only warn you about being unused. This means that it recognizes our new types, and we can add some properties to them.

** Add some properties to our new classes**
I decided that the name is going to be a pair of strings stored in an array, and the phone number and e-mail will just be strings. We can add any complications we want here, such as breaking the email down into name and domain or breaking the phone up into area code and phone number. For  now we'll keep it simple.

```java
    static public class Name{
        private String[] value = new String[2];
    }
    
    static public class Phone{
        private String value;
    }
    
    static public class Email{
        private String value;
    }
```

At this point we have the form for our data, we know how we want to store the objects. Now we need the logic to back it up and that's where the constructors and other methods are going to come in.
## What is a constructor?
So something really handy about classes is the ability have a constructor. You see a constructor in action when you say something like `New Integer(11);` in that case, the 11 is in brackets because it is a parameter being fed into a function call. More specifically a call to the constructor of the Integer class.

We're going to add constructors to all of our classes to make it easy to whip up new contacts and their properties and have the constructors do the heavy lifting of validating and organizing things. Constructors must have the same method name as the class. It is possible to overload constructors (provide multiple versions of the same method which have different arguments and bodies).
**Add a constructor class to each of the classes**
```java
public class Contact {

    public Contact(String firstName, String lastName,String cPhone, String cEmail)
    {
        contactName = new Name(firstName, lastName);
        contactPhone = new Phone(cPhone);
        contactEmail = new Email(cEmail);
    }
    
    static public class Name{
        private String[] value = new String[2];
        
        public Name(String first, String last)
        {
            value[0] = first;
            value[1] = last;
        }
    }
    
    static public class Phone{
        private String value;
        
        public Phone(String number)
        {
            value = number;
        }
    }
    
    
    static public class Email{
        private String value;
        
        public Email(String email)
        {
            value = email;
        }
    }

    private Name contactName;
    private Phone contactPhone;
    private Email contactEmail;
}
```
All these constructors do is assign the value you provide in the function to the value of that object. These are very simple constructors, let's take a look at some examples of creating objects given the rules we've laid out here
```JAVA
//These examples assume that we are within the contact class.

//Create a new Name to be put into a contact
Name nameVar = new Name("Bob", "Stevens");

//Create a new Email to be put into a contact;
Email emailVar = new Email("bobStevens@bobstevens.com");

//Create a new Phone number to be put into a contact:
Phone phoneVar = new Phone("555-5512-2251");

//Take these variables and stuff them into a new Contact
Contact newContact = new Contact (nameVar, phoneVar, emailVar);
```
**Adding Getters and Setters**
When we use objects to hold data in this way, it is very common to make the variables of the class private and create accessor functions, getters and setters to read and write to the data.

```java
//Getter Setter Pattern
class SomeClass{
	var someProperty;
	//Notice the return type is the same as the property type
	//no arguments
	public var GetSomeProperty()
	{
		return someProperty;
	}
	//argument
	//No need for a return type as we are setting a property
	public void SetSomeProperty(var newValue)
	{
		someProperty = newValue;
	}
}
```

## Add Getters and Setters to the Name, Email, and Phone classes

### Name class setters and getters
I added a few methods to the name for getting because I thought it might be useful to be able to pull the first name, last name, or full name with a method call on the object. If we weren't just having some fun it would be a good idea to tailor your methods to your requirements, but let's show off what we can do.

```java
    static public class Name{
        private String[] value = new String[2];
        
        public Name(String first, String last)
        {
            value[0] = first;
            value[1] = last;
        }
        
        public String GetFullName()
        {
            return value[0] + " " + value[1];
        }
        public String GetFirstName()
        {
            return value[0];
        }
        public String GetLastName()
        {
            return value[1];
        }
        
        public void SetName(String first, String last)
        {
            if(first != null)
                value[0] = first;
            if(last != null)
                value[1] = last;
        }
    }
```

The new methods here are GetFullName(), GetFirstName(), GetLastName() and SetName(). The getters are set up to do exactly what they say on the tin and they just return whatever part of the value you've requested. The setter is a little bit tricky in that it checks if you've provided null for either the first or last name and then refuses to update that value. So that you'd be able to update only the last name if you wanted to.

### Getters and Setters for Phone
Email and phone become quite simple and show the classic pattern of get-set very well.
```java
    static public class Phone{
        private String value;
        
        public Phone(String number)
        {
            value = number;
        }
        
        public String GetNumber()
        {
            return value;
        }
        
        public void SetNumber(String newNumber)
        {
            value = newNumber;
        }
    }
```

### Getters and Setters for Email
The pattern should remain the same, notice that I'm making the names of the parameters and methods different instead of giving each a 'GetValue();' and a 'SetValue();' method. I'm doing this so that when we go to play with these methods from outside of the class they appear distinctly from one another.

Once we can play with tools like inheritance we might want to create a 'ContactField' class that the Name, Phone and Email all inherit from which provides the getters and setter methods and value attribute all from one place. For now though, we're going to write a few different getters and setters to get some practice.
```java
    static public class Email{
        private String value;
        
        public Email(String email)
        {
            value = email;
        }
        public String GetAddress()
        {
            return value;
        }
        
        public void SetAddress(String newAddress)
        {
            value = newAddress;
        }
    }
```

## Finishing up with the Contact class, adding our front facing methods.
We've built a  bunch of framework to play with the fields within a contact. These are the bare bones, we don't have the business logic validating e-mails or phone numbers or imposing any restrictions on entry, this is because all of that junk would triple this already too-long tutorial!

Let's add a few functions at the very end of the Contact class, just below our three private contactInfo variables.
First, the ability to change the data inside of a contact from outside of the contact. This is kind of the creation of our API. I'm using function overloading here so that you can call SomeContact.UpdateContact(); and in the brackets provide either a name object, email object, or phone object, to change the different fields.

This is called **static polymorphism**.
```java
    public void UpdateContact(Name newName)
    {
        contactName = newName;
    }
    public void UpdateContact(Email newEmail)
    {
        contactEmail = newEmail;
    }
    public void UpdateContact(Phone newPhone)
    {
        contactPhone = newPhone;
    }
```
Add this method below, this retrieves everything we know about the contact and packages it up into an array of strings.
```java
    public String[] GetReport()
    {
        String[] ContactData = new String[4];
        ContactData[0] = contactName.GetFirstName();
        ContactData[1] = contactName.GetLastName();
        ContactData[2] = contactPhone.GetNumber();
        ContactData[3] = contactEmail.GetAddress();
        return ContactData;
    }
```
Check out that grand slam of getters. We're using all of the methods we've created to get the information from the objects within. All of these different layers of access and protection may seem crazy, but we're creating layers of access and different languages, rules and paradigms that we will follow within our own program.

You aren't just writing a program, you're writing the language that determines how your program will be written moving forwards. This is why everyone is telling you about the importance of proper planning.

With that being said let's move on with flying by the seat of our pants.
Let's also add a method that blurts out the information for the contact if we happen to want it all at once.
```java
    public void PrintReport()
    {
        String[] contact = GetReport();
        System.out.println("Contact Name: \t" + contact[0] + contact[1]);
        System.out.println("Contact Phone: \t" + contact[2]);
        System.out.println("Contact Email: \t" + contact[3]);
    }
```
### Quick review: Final source code for Contact.Java
```java
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saltcallage.contactclasses;

/**
 *
 * @author mike
 */
public class Contact {

    public Contact(String firstName, String lastName,String cPhone, String cEmail)
    {
        contactName = new Name(firstName, lastName);
        contactPhone = new Phone(cPhone);
        contactEmail = new Email(cEmail);
    }
    

    
    static public class Name{
        private String[] value = new String[2];
        
        public Name(String first, String last)
        {
            value[0] = first;
            value[1] = last;
        }
        
        public String GetFullName()
        {
            return value[0] + " " + value[1];
        }
        public String GetFirstName()
        {
            return value[0];
        }
        public String GetLastName()
        {
            return value[1];
        }
        
        public void SetName(String first, String last)
        {
            if(first != null)
                value[0] = first;
            if(last != null)
                value[1] = last;
        }
    }
    
    static public class Phone{
        private String value;
        
        public Phone(String number)
        {
            value = number;
        }
        
        public String GetNumber()
        {
            return value;
        }
        
        public void SetNumber(String newNumber)
        {
            value = newNumber;
        }
    }
    
    
    static public class Email{
        private String value;
        
        public Email(String email)
        {
            value = email;
        }
        public String GetAddress()
        {
            return value;
        }
        
        public void SetAddress(String newAddress)
        {
            value = newAddress;
        }
    }

    private Name contactName;
    private Phone contactPhone;
    private Email contactEmail;
    
    public void UpdateContact(Name newName)
    {
        contactName = newName;
    }
    public void UpdateContact(Email newEmail)
    {
        contactEmail = newEmail;
    }
    public void UpdateContact(Phone newPhone)
    {
        contactPhone = newPhone;
    }
    
    public String[] GetReport()
    {
        String[] ContactData = new String[4];
        ContactData[0] = contactName.GetFirstName();
        ContactData[1] = contactName.GetLastName();
        ContactData[2] = contactPhone.GetNumber();
        ContactData[3] = contactEmail.GetAddress();
        return ContactData;
    }
    
    public void PrintReport()
    {
        String[] contact = GetReport();
        System.out.println("Contact Name: \t" + contact[0] + contact[1]);
        System.out.println("Contact Phone: \t" + contact[2]);
        System.out.println("Contact Email: \t" + contact[3]);
    }
}
```
# It is finally time to move on to the addressbook class
This one should be easier, we're just going to make some lists to hold our contacts and try making some contacts and create some functions to actually print the reports on the screen. Let's finally dig into the addressbook class.

**Create a new ContactBook class within the new package you created**

You should have something like this to start with:

**Check under the package declaration and add the imports I have added**
```Java
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saltcallage.contactclasses;

import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author mike
 */
public class ContactBook {
    
}
```
# Lists in Java: Something you should look at
Take some time to look into them, we are going to be using a List as the first property added to our ContactBook class:
```java
public class ContactBook {
    private List<Contact> AllContacts = new ArrayList<Contact>();
}
```

**Add a methods which add, remove, retrieve contacts on your contact list.**
```java
    public void RegisterContact(Contact newContact)
    {
        AllContacts.add(newContact);
    }
    
    public void RemoveContact(Contact toRemove)
    {
        AllContacts.remove(toRemove);
    }
    
    public List<Contact> GetAllContacts()
    {
        return AllContacts;
    }
```

**Bonus methods: Add some methods for finding particular contacts**
Make sure you insert these as well. These methods are going to be used to pull specific contacts later on down the line. Consider it a personal challenge to figure out exactly what's going on with GetFirstName(), and to advance even further to take a look at FindAllContactsByLastName(). I have documented GetFirstName to try and assist.	
```java
    public Contact GetContact(int cid)
    {
        if(cid < AllContacts.size())
        {
            return AllContacts.get(cid);
        }
        else
        {
            return null;
        }
    }

    //We're returning a list of Contacts with this function
    public List<Contact> GetFirstName(String firstName)
    {
        //We have to set up our new list
        List <Contact> foundContacts = new ArrayList<Contact>();
        //This is an enhanced for loop, looking through all contacts
        for(Contact contact : AllContacts)
        {
            //Remember what contact.GetReport does, what is element 0?
            //What is equalsIgnoreCase? a method call as a part of String.
            if(contact.GetReport()[0].equalsIgnoreCase(firstName))
                foundContacts.add(contact);
        }
        return foundContacts;
    }
    
    public void PrintAllContactsByLastName()
    {
        ArrayList<String> lastNames = new ArrayList<String>();
        for(Contact element : AllContacts)
        {
            String[] cd = element.GetReport();
            lastNames.add(cd[1] + ", " + cd[0] + ":" + cd[2] + "(" + AllContacts.indexOf(element) + ")");
        }
        Collections.sort(lastNames);
        
        for(String element : lastNames)
            System.out.println(element);
    }
```
### Quick review, final code for ContactBook.java
```java
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saltcallage.contactclasses;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author mike
 */
public class ContactBook {
    private List<Contact> AllContacts = new ArrayList<Contact>();

    public void RegisterContact(Contact newContact)
    {
        AllContacts.add(newContact);
    }
    
    public void RemoveContact(Contact toRemove)
    {
        AllContacts.remove(toRemove);
    }
    
    public List<Contact> GetAllContacts()
    {
        return AllContacts;
    }
    
    public Contact GetContact(int cid)
    {
        if(cid < AllContacts.size())
        {
            return AllContacts.get(cid);
        }
        else
        {
            return null;
        }
    }
    
    //We're returning a list of Contacts with this function
    public List<Contact> GetFirstName(String firstName)
    {
        //We have to set up our new list
        List <Contact> foundContacts = new ArrayList<Contact>();
        //This is an enhanced for loop, looking through all contacts
        for(Contact contact : AllContacts)
        {
            //Remember what contact.GetReport does, what is element 0?
            //What is equalsIgnoreCase? a method call as a part of String.
            if(contact.GetReport()[0].equalsIgnoreCase(firstName))
                foundContacts.add(contact);
        }
        return foundContacts;
    }
    
    public void PrintAllContactsByLastName()
    {
        ArrayList<String> lastNames = new ArrayList<String>();
        for(Contact element : AllContacts)
        {
            String[] cd = element.GetReport();
            lastNames.add(cd[1] + ", " + cd[0] + ":" + cd[2] + "(" + AllContacts.indexOf(element) + ")");
        }
        Collections.sort(lastNames);
        
        for(String element : lastNames)
            System.out.println(element);
    }
}
```
## Finally, let's write a test.
 Create a new java main class, but create it in the package that was generated with the project. NOT the package that you've created your contact classes in. Name it AddressTest and verify that your boilerplate code looks like mine.
```Java
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saltcallage.addressbook;

/**
 *
 * @author mike
 */
public class AddressTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}

```

**Import the package that we've created to get access to the controls we've made**
Take a look at the snip here to see where
```java
package com.saltcallage.addressbook;

import com.saltcallage.contactclasses.ContactBook; //<
import com.saltcallage.contactclasses.Contact; //<
import java.util.List; //<
/**
 *
 * @author mike
 */
public class AddressTest {
```

**Test Part one: Create our address book**
```java
    public static void main(String[] args) {
        // TODO code application logic here
        ContactBook testBook = new ContactBook();
```
**Test Part two: Create some sample contacts**
I've created the sample data for you, we're going to look at automatically  generating this crap but for now we want something that looks believable.
```java
        Contact[] sampleData = new Contact[10];
        
        sampleData[0] = new Contact("Ray", "Jones", "555-251-2851", "ray@jones.com");
        sampleData[1] = new Contact("Alex", "Jones", "555-661-2271", "AlexJones@jones.com");
        sampleData[2] = new Contact("Bill", "Ford", "555-221-2251", "BillyBoy@StevesComps.com");
        sampleData[3] = new Contact("Ray", "Jones", "555-221-2612", "raymon@DoubleJones.com");
        sampleData[4] = new Contact("Linden", "Stevens", "555-221-6271", "L.S@Stevens.edu");
        sampleData[5] = new Contact("Mike", "Michaels", "555-221-2211", "MM@MM.MM");
        sampleData[6] = new Contact("Steve", "Stone", "555-221-2331", "Stevey@StonesSomething.com");
        sampleData[7] = new Contact("Ray", "Ford", "555-263-2671", "OhDear@FordFord.com");
        sampleData[8] = new Contact("Pedo", "Bear", "555-663-6611", "Youknow@Theway.com");
        sampleData[9] = new Contact("Goku", "Ginka", "555-226-2267", "Goku@kamehameha.com");
```
**Test Part three: Add the contacts to the addressbook**
Using the method we created in the addressbook class, we can now register each of the contacts we've created into the book.
```java
        for(Contact element : sampleData)
        {
            testBook.RegisterContact(element);
        }
```

**Test Part four: Finally, FINALLY PRINT SOMETHING!**
Now that our contactbook 'testBook' has been populated with the data, let's try out the "PrintAllContactsByLastName();" method.
```java
	System.out.println("---Initiating sort by last name and print test---");
        testBook.PrintAllContactsByLastName();
        System.out.println("--- END of sort by last name test ---")
```
After you get this main function done, you can actually see something come out of the compile process:
```
--- exec-maven-plugin:1.2.1:exec (default-cli) @ AddressBook ---
Initiating sort by last name and print test
Bear, Pedo:555-663-6611(8)
Ford, Bill:555-221-2251(2)
Ford, Ray:555-263-2671(7)
Ginka, Goku:555-226-2267(9)
Jones, Alex:555-661-2271(1)
Jones, Ray:555-221-2612(3)
Jones, Ray:555-251-2851(0)
Michaels, Mike:555-221-2211(5)
Stevens, Linden:555-221-6271(4)
Stone, Steve:555-221-2331(6)
--- END of sort by last name test ---
------------------------------------------------------------------------
BUILD SUCCESS
```
Look at that, we have all ten contacts and they are being sorted properly by last name. 

**Test Part five: Fix a mistake**
Looks like entry number 6, Stone has the wrong phone, so we're going to create some replacement data for it and insert it into the entry.
```java
        System.out.println("\n---Initiating repair the stone phone test---");
        Contact bear = testBook.GetContact(6);
        System.out.println("Original Entry:");
        bear.PrintReport();
        bear.UpdateContact(new Contact.Phone("555-999-9999"));
        System.out.println("New Entry:");
        bear.PrintReport();
        System.out.println("---End of repair stone phone test---");
```
This test takes contact number 6 and prints it out, makes a modification to the phone number by providing a new phone number to the UpdateContact() method and prints it out one more time to confirm the change, you should see this in your output:
```java
Initiating repair the stone phone test
Original Entry:
Contact Name: 	SteveStone
Contact Phone: 	555-221-2331
Contact Email: 	Stevey@StonesSomething.com
New Entry:
Contact Name: 	SteveStone
Contact Phone: 	555-999-9999
Contact Email: 	Stevey@StonesSomething.com
---End of repair stone phone test---
```
**Test Part six: Find everyone named Ray**
Lets use another bonus method from the addressbook to show off filtering by name:
```java
	System.out.println("\n--- Initializing FIND RAY test ---");
        List<Contact> Rays = testBook.GetFirstName("Ray");
        
        for(Contact element : Rays)
        {
            element.PrintReport();
        }
        System.out.println("--- END of FIND RAY test ---");
        
```
You should find three:
```
--- Initializing FIND RAY test ---
Contact Name: 	RayJones
Contact Phone: 	555-251-2851
Contact Email: 	ray@jones.com
Contact Name: 	RayJones
Contact Phone: 	555-221-2612
Contact Email: 	raymon@DoubleJones.com
Contact Name: 	RayFord
Contact Phone: 	555-263-2671
Contact Email: 	OhDear@FordFord.com
--- END of FIND RAY test ---
------------------------------------------------------------------------
BUILD SUCCESS
```
### Final review, code for AddressTest.java
```java
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saltcallage.addressbook;

import com.saltcallage.contactclasses.ContactBook;
import com.saltcallage.contactclasses.Contact;
import java.util.List;
/**
 *
 * @author mike
 */
public class AddressTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ContactBook testBook = new ContactBook();
        
        Contact[] sampleData = new Contact[10];
        
        sampleData[0] = new Contact("Ray", "Jones", "555-251-2851", "ray@jones.com");
        sampleData[1] = new Contact("Alex", "Jones", "555-661-2271", "AlexJones@jones.com");
        sampleData[2] = new Contact("Bill", "Ford", "555-221-2251", "BillyBoy@StevesComps.com");
        sampleData[3] = new Contact("Ray", "Jones", "555-221-2612", "raymon@DoubleJones.com");
        sampleData[4] = new Contact("Linden", "Stevens", "555-221-6271", "L.S@Stevens.edu");
        sampleData[5] = new Contact("Mike", "Michaels", "555-221-2211", "MM@MM.MM");
        sampleData[6] = new Contact("Steve", "Stone", "555-221-2331", "Stevey@StonesSomething.com");
        sampleData[7] = new Contact("Ray", "Ford", "555-263-2671", "OhDear@FordFord.com");
        sampleData[8] = new Contact("Pedo", "Bear", "555-663-6611", "Youknow@Theway.com");
        sampleData[9] = new Contact("Goku", "Ginka", "555-226-2267", "Goku@kamehameha.com");
       
        for(Contact element : sampleData)
        {
            testBook.RegisterContact(element);
        }
        
  	System.out.println("\n---Initiating sort by last name and print test---");      
        testBook.PrintAllContactsByLastName();
        System.out.println("--- END of sort by last name test ---");
        
        System.out.println("\n---Initiating repair the stone phone test---");
        Contact bear = testBook.GetContact(6);
        System.out.println("Original Entry:");
        bear.PrintReport();
        bear.UpdateContact(new Contact.Phone("555-999-9999"));
        System.out.println("New Entry:");
        bear.PrintReport();
        System.out.println("---End of repair stone phone test---");
        
        System.out.println("\n--- Initializing FIND RAY test ---");
        List<Contact> Rays = testBook.GetFirstName("Ray");
        
        for(Contact element : Rays)
        {
            element.PrintReport();
        }
        System.out.println("--- END of FIND RAY test ---");
    }
}
```
# Tutorial Complete!
This was an intense one, if you're doing this assignment, make sure to create a branch and submit your finished and working project and set up the merge request!



## Next Steps (Bonus Marks):
- Extend the program so that you can also search for entries by last name as well as first
